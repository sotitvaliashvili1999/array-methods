//  N1 - ARRAY METHODS
const whoArefriends = array => {
  const friends = array.filter(name => name.length === 4);
  return friends.length > 0 ? friends : "No one is your friend";
};
const people = ["George", "Nick", "Tom", "Kate", "Annie"];
console.log(whoArefriends(people));

// N1  without ARRAY METHODS

const whoAreFriends = array => {
  const friends = [];
  let cnt = 0;
  for (let i = 0; i < array.length; i++) {
    if (array[i].length === 4) {
      friends[cnt] = array[i];
      cnt++;
    }
  }
  return cnt > 0 ? friends : "No one is your friend";
};

const people = ["George", "Nick", "Tom", "Kate", "Annie"];
console.log(whoArefriends(people));

//  N2 - ARRAY METHODS

const sumTwoLowestPosNum = array => {
  array.sort((a, b) => a - b);
  const onlyPosNum = array.filter(number => number > 0);
  return onlyPosNum.length > 0
    ? onlyPosNum[0] + onlyPosNum[1]
    : "Array has not 2 or more positive numbers";
};
const numbers = [52, 76, 14, 12, 4];
console.log(sumTwoLowestPosNum(numbers));

// N2  without ARRAY METHODS

const sort = array => {
  let tmp = 0;
  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] > array[j]) {
        tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
      }
    }
  }
};
const sumTwoLowestPosNum = array => {
  sort(array);
  for (let i = 0; i < array.length; i++) {
    if (array[i] > 0) {
      for (let j = i + 1; j < array.length; j++)
        if (array[j] > 0) return array[i] + array[j];
    }
    if (i == array.length - 1) return "Array has not two positive numbers";
  }
};
const numbers = [52, 76, 14, 12, 4];
console.log(sumTwoLowestPosNum(numbers));
